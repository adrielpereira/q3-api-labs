const express = require('express')
const router = express.Router()
const rounds = require('../../mock/db.json')
const Boom = require('boom')

router.get('/', (req, res, next) => {
  res.json(rounds)
})

router.get('/:id', (req, res, next) => {
  let round = rounds.find(x => x.id === parseInt(req.params.id))
  if (!round) {
    throw Boom.notFound('record not found')
  }
  res.json(round)
})

module.exports = router

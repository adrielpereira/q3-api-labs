const express = require('express')
const router = express.Router()
const rounds = require('./resources/rounds/routes')

router.use('/rounds', rounds)

module.exports = router

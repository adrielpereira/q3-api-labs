const errorHandler = (err, req, res, next) => {
  if (err.isBoom) {
    res.status(err.output.statusCode).json({ message: err.output.payload.message })
  } else {
    res.status(err.status)
    res.render({ message: err.message })
  }
}

const notFound = (req, res, next) => {
  res.status(404).json({ message: 'resource not found' })
}

module.exports = { notFound, errorHandler }

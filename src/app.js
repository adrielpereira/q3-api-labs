const express = require('express')
const helmet = require('helmet')
const routes = require('./routes')
const errorMiddleware = require('./middlewares/error').errorHandler
const notFoundMiddleware = require('./middlewares/error').notFound

const app = express()

// Middlewares
app.use(express.json())
app.use(helmet())

// Routes
app.use('/api', routes)

// Errors middleware
app.use(errorMiddleware)
app.use(notFoundMiddleware)

const port = process.env.PORT || 3000
app.listen(port, () => console.log(`Server is runing ${port}`))

module.exports = app

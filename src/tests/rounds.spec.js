const app = require('../app')
const expect = require('chai').expect
const assert = require('chai').assert
const request = require('supertest')
const roundResponse = {
  id: 2,
  startAt: '0:00',
  endAt: '1:47',
  serverName: 'Code Miner Server',
  totalKills: 4,
  players: [
    'Isgalamido',
    'Mocinha',
    'Zeh',
    'Dono da Bola'
  ],
  kills: [
    {
      player: 'Isgalamido',
      kill: 1
    },
    {
      player: 'Zeh',
      kill: -2
    },
    {
      player: 'Dono da Bola',
      kill: -1
    }
  ],
  deathsBy: [
    {
      mod: 'MOD_ROCKET',
      total: 1
    },
    {
      mod: 'MOD_TRIGGER_HURT',
      total: 2
    },
    {
      mod: 'MOD_FALLING',
      total: 1
    }
  ]
}
describe('Url', () => {
  describe('#GET /non-existent url', function () {
    it('should return status 404 and message `resource not found`', function (done) {
      request(app).get('/api/nonexistent')
        .end(function (err, res) {
          expect(res.statusCode).to.equal(404)
          expect(res.body.message).to.be.equal('resource not found')
          done()
        })
    })
  })
})

describe('Rounds', () => {
  describe('#GET /rounds', function () {
    it('should get all rounds', function (done) {
      request(app).get('/api/rounds')
        .end(function (err, res) {
          expect(res.statusCode).to.equal(200)
          expect(res.body).to.be.an('array')
          done()
        })
    })
  })

  describe('#GET /rounds/:id', function () {
    it('should be a object with keys and values', function (done) {
      request(app).get('/api/rounds/1')
        .end(function (err, res) {
          expect(res.body).to.have.property('startAt')
          expect(res.body.startAt).to.not.equal(null)
          expect(res.body).to.have.property('endAt')
          expect(res.body.endAt).to.not.equal(null)
          expect(res.body).to.have.property('serverName')
          expect(res.body.serverName).to.not.equal(null)
          expect(res.body).to.have.property('totalKills')
          expect(res.body.totalKills).to.not.equal(null)
          expect(res.body.players).to.be.an('array')
          expect(res.body.kills).to.be.an('array')
          expect(res.body.deathsBy).to.be.an('array')
          done()
        })
    })
  })
  describe('#GET /rounds/:id', function () {
    it('should be a object with keys and values `assert test`', function (done) {
      request(app).get('/api/rounds/2')
        .end(function (err, res) {
          assert.deepEqual(res.body, roundResponse)
          done()
        })
    })
  })
  describe('#GET /rounds/99', function () {
    it('should return status 404 and message `record not found`', function (done) {
      request(app).get('/api/rounds/99')
        .end(function (err, res) {
          expect(res.statusCode).to.equal(404)
          expect(res.body.message).to.be.equal('record not found')
          done()
        })
    })
  })
})

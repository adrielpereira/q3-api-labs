# Q3 API Labs

> A Q3 API to explore Quake 3 rounds stats.

The main idea is use [q3-parser-labs](https://gitlab.com/adrielpereira/q3-parser-labs) to parse a log file from Quake 3 and use the API to expose it.

### Background
[Nodejs v8.11.4](https://nodejs.org/en/)
[Npm v5.6.0](https://www.npmjs.com/)
Recommend to use Node >= 8 and npm >= 3.

### Install
For now, you can clone this repo and run.

```
npm install
```
To install all dependencies.

### Running the server

To put the server up is easy.

```
npm start
```
## API

The API is documented using [API Blueprint](https://apiblueprint.org/) and the docs can be seen here:
[API Docs](./docs/q3-api-labs.md)

#### Running the tests

- `npm test`: run your tests in a single-run mode.
- `npm run test:tdd`: run and keep watching your test files.

#### Linting your code
In order to keep the code clean and consistent use [eslint](http://eslint.org/) with [Standard preset](https://standardjs.com/)

- `npm run lint`: lint all files searching for errors.
- `npm run lint:fix`: fix automaticaly some lint errors.

#### Style Guide

- [EditorConfig](http://editorconfig.org/) - *standardize some general settings among multiple editors*
- [ESLint](http://eslint.org/) - *for reporting the patterns of code*
  - [Javascript Standard](https://standardjs.com)

#### Tests
- [Mocha](https://github.com/mochajs/mocha) - *test framework*
- [Chai](https://github.com/chaijs/chai) - *assertions*
- [Supertest](https://github.com/visionmedia/supertest)

FORMAT: 1A

# q3-api-labs

A simple API to expose Quake3 rounds stats.

## Rounds Collection [/api/rounds]
### Get rounds [GET /api/rounds]
+ Response 200 (application/json)
```
[
   {
      "id":1,
      "startAt":"0:00",
      "endAt":"1:47",
      "serverName":"Code Miner Server",
      "totalKills":4,
      "players":[
         "Isgalamido",
         "Mocinha",
         "Zeh",
         "Dono da Bola"
      ],
      "kills":[
         {
            "player":"Isgalamido",
            "kill":1
         },
         {
            "player":"Zeh",
            "kill":-2
         },
         {
            "player":"Dono da Bola",
            "kill":-1
         }
      ],
      "deathsBy":[
         {
            "mod":"MOD_ROCKET",
            "total":1
         },
         {
            "mod":"MOD_TRIGGER_HURT",
            "total":2
         },
         {
            "mod":"MOD_FALLING",
            "total":1
         }
      ]
   },
   {
      "id":2,
      "startAt":"0:00",
      "endAt":"1:47",
      "serverName":"Code Miner Server",
      "totalKills":4,
      "players":[
         "Isgalamido",
         "Mocinha",
         "Zeh",
         "Dono da Bola"
      ],
      "kills":[
         {
            "player":"Isgalamido",
            "kill":1
         },
         {
            "player":"Zeh",
            "kill":-2
         },
         {
            "player":"Dono da Bola",
            "kill":-1
         }
      ],
      "deathsBy":[
         {
            "mod":"MOD_ROCKET",
            "total":1
         },
         {
            "mod":"MOD_TRIGGER_HURT",
            "total":2
         },
         {
            "mod":"MOD_FALLING",
            "total":1
         }
      ]
   },
   {
      "id":3,
      "startAt":"1:47",
      "endAt":"12:13",
      "serverName":"Code Miner Server",
      "totalKills":105,
      "players":[
         "Isgalamido",
         "Dono da Bola",
         "Zeh",
         "Assasinu Credi"
      ],
      "kills":[
         {
            "player":"Isgalamido",
            "kill":19
         },
         {
            "player":"Dono da Bola",
            "kill":13
         },
         {
            "player":"Zeh",
            "kill":20
         },
         {
            "player":"Assasinu Credi",
            "kill":13
         }
      ],
      "deathsBy":[
         {
            "mod":"MOD_TRIGGER_HURT",
            "total":9
         },
         {
            "mod":"MOD_FALLING",
            "total":11
         },
         {
            "mod":"MOD_ROCKET",
            "total":20
         },
         {
            "mod":"MOD_RAILGUN",
            "total":8
         },
         {
            "mod":"MOD_ROCKET_SPLASH",
            "total":51
         },
         {
            "mod":"MOD_MACHINEGUN",
            "total":4
         },
         {
            "mod":"MOD_SHOTGUN",
            "total":2
         }
      ]
   }
```

### Get round by Id [GET /api/rounds/{id}]
+ id: 4 (number, required)
+ Response 200 (application/json)
```
{
  "id":4,
  "startAt":"0:00",
  "endAt":"1:47",
  "serverName":"Code Miner Server",
  "totalKills":4,
  "players":[
     "Isgalamido",
     "Mocinha",
     "Zeh",
     "Dono da Bola"
  ],
  "kills":[
     {
        "player":"Isgalamido",
        "kill":1
     },
     {
        "player":"Zeh",
        "kill":-2
     },
     {
        "player":"Dono da Bola",
        "kill":-1
     }
  ],
  "deathsBy":[
     {
        "mod":"MOD_ROCKET",
        "total":1
     },
     {
        "mod":"MOD_TRIGGER_HURT",
        "total":2
     },
     {
        "mod":"MOD_FALLING",
        "total":1
     }
  ]
}
```
